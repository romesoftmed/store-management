import axios from "axios";
import settings from "../helpers/config";

const getAllProducts = async () => {
  const { data } = await axios({
    method: "GET",
    url: `${settings.API_URL}/product`,
  });

  return data;
};

const getProductsByCategory = async (params: any) => {
  const { data } = await axios({
    method: "GET",
    url: `${settings.API_URL}/product/category`,
    params,
  });

  return data;
};

const createProduct = async (product: any) => {
  const { data } = await axios({
    method: "POST",
    url: `${settings.API_URL}/product`,
    data: { product },
  });

  return data;
};

const updateProduct = async ({ sku, product }: any) => {
  const { data } = await axios({
    method: "PUT",
    url: `${settings.API_URL}/product`,
    params: { sku },
    data: { product },
  });

  return data;
};

export { getAllProducts, getProductsByCategory, createProduct, updateProduct };
