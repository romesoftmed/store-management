import axios from "axios";
import settings from "../helpers/config";
import { category } from "../helpers/types";

const getAllCategories = async () => {
  const { data } = await axios({
    method: "GET",
    url: `${settings.API_URL}/category/all`,
  });

  return data;
};

const createCategory = async (category: category) => {
  const { data } = await axios({
    method: "POST",
    url: `${settings.API_URL}/category`,
    data: { category },
  });

  return data;
};

const updateCategory = async (category: category) => {
  const { data } = await axios({
    method: "PUT",
    url: `${settings.API_URL}/category`,
    data: { category },
  });

  return data;
};

export { getAllCategories, createCategory, updateCategory };
