export interface category {
  id: string;
  name: string;
  icon: string;
}

export interface product {
  sku: string;
  name: string;
  size: number;
  unit: string;
  cost: number;
  price: number;
  img: string;
  discount: number;
  stock: number;
  category: string;
}
