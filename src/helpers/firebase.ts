import { initializeApp } from "firebase/app";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyADZdgiB3mJcYXcJgLCAt7oFNcuITjmPOI",
  authDomain: "virtual-shop-7bf6d.firebaseapp.com",
  projectId: "virtual-shop-7bf6d",
  storageBucket: "virtual-shop-7bf6d.appspot.com",
  messagingSenderId: "516158125966",
  appId: "1:516158125966:web:4d5963bac8eb8f568c16df",
};

export const app = initializeApp(firebaseConfig);
export const storage = getStorage(app);
