export const defaultProduct = {
  sku: "",
  name: "",
  size: 0,
  unit: "",
  cost: 0,
  price: 1,
  img: "",
  discount: 0,
  stock: 0,
  category: "",
};

export const defaultCategory = {
  id: "",
  name: "",
  icon: "",
};
