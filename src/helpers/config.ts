const { REACT_APP_API_URL } = process.env;

const settings = {
  API_URL: REACT_APP_API_URL,
};

export default settings;
