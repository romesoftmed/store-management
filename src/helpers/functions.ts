export const getPrice = ({percentage, cost}: any) => {
   return formatNumber(((cost/(1-(percentage/100))))/1);
}

export const getPercentage = ({price, cost}: any) => {
   return formatNumber(((price - cost) / price) * 100);
}

export const formatNumber = (number: number) => {
   return Math.round(number * 1)/1;
}
