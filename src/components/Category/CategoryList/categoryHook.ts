import { useEffect, useState } from "react";
import { getAllCategories } from "../../../api/category";
import { category } from "../../../helpers/types";

const useCategory = () => {
  const [categories, setCategories] = useState<category[]>();
  const [status, setStatus] = useState<string>("init");

  useEffect(() => {
    setStatus("loading");
    getAllCategories()
      .then((categories) => {
        setCategories(categories);
        setStatus("loaded");
      })
      .catch(() => {
        setStatus("error");
      });
  }, []);

  return { categories, status };
};

export default useCategory;
