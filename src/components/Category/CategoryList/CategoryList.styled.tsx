import styled from "styled-components";
import CategoryList from "./CategoryList";

const CategoryListStyled = styled(CategoryList)`
  overflow-y: scroll;

  table,
  th,
  td {
    padding: 4px;
    text-align: justify;
    border: 1px solid #e7e7e7;
  }

  td {
    text-align: center;
  }

  table {
    overflow: scroll;
  }

  img {
    width: 30px;
  }
`;

export default CategoryListStyled;
