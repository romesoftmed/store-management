import { memo } from "react";
import useCategory from "./categoryHook";
import CategoryEditor from "../CategoryEditor";
import NewCategory from "../NewCategory";
import { storage } from "../../../helpers/firebase";
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
import { createCategory, updateCategory } from "../../../api/category";
import { category } from "../../../helpers/types";

interface Props {
  className?: string;
}

const CategoryList = ({ className }: Props) => {
  const { categories, status } = useCategory();

  const create = ({ file, category }: any) => {
    if (file) {
      const storageRef = ref(storage, `category/${category.id}.svg`);
      uploadBytes(storageRef, file).then(() => {
        getDownloadURL(storageRef).then((icon) => {
          createInDatabase({ ...category, icon });
        });
      });
    }
  };

  const update = ({ file, category }: any) => {
    if (file) {
      const storageRef = ref(storage, `category/${category.id}.svg`);
      uploadBytes(storageRef, file).then(() => {
        updateInDatabase(category);
      });
    } else {
      updateInDatabase(category);
    }
  };

  const createInDatabase = (category: category) => {
    createCategory(category)
      .then(() => {})
      .catch(() => {});
  };

  const updateInDatabase = (category: category) => {
    updateCategory(category)
      .then(() => {})
      .catch(() => {});
  };

  return (
    <div className={className}>
      {status === "loading" && <h1>Cargando ...</h1>}
      {status === "error" && <h1>Hubo un error</h1>}
      {status === "loaded" && categories && (
        <table>
          <tbody>
            <tr>
              <th></th>
              <th>Imagen</th>
              <th>ID</th>
              <th>Nombre</th>
            </tr>
            <NewCategory createCategory={create} />
            {categories.map((category) => (
              <CategoryEditor
                key={category.id}
                {...category}
                updateCategory={update}
              />
            ))}
          </tbody>
        </table>
      )}
    </div>
  );
};

export default memo(CategoryList);
