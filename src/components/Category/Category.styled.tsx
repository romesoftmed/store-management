import styled from "styled-components";
import Category from "./Category";

const CategoryStyled = styled(Category)`
  height: calc(95vh - 30px);
  margin: 10px;
  display: grid;
`;

export default CategoryStyled;
