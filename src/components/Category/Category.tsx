import { memo } from "react";
import CategoryList from "./CategoryList";

interface Props {
  className?: string;
}

const Category = ({ className }: Props) => {
  return (
    <div className={className}>
      <h3>Gestionar categorías</h3>
      <CategoryList />
    </div>
  );
};

export default memo(Category);
