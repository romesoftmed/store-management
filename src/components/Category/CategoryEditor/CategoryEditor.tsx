import { memo, useEffect, useRef, useState } from "react";
import FileUploader from "../../FileUploader";
import { category } from "../../../helpers/types";

interface Props {
  id: string;
  name: string;
  icon: string;
  updateCategory: Function;
}

const CategoryEditor = ({ id, name, icon, updateCategory }: Props) => {
  const [edit, setEdit] = useState<boolean>(false);
  const [category, setCategory] = useState<category>({
    id: "",
    name: "",
    icon: "",
  });

  const fileRef = useRef<any>(null);

  useEffect(() => {
    setCategory({ id, name, icon });
    // eslint-disable-next-line
  }, []);

  const toogleEdit = () => {
    setEdit(!edit);
  };

  const handleChange = (e: any) => {
    const value = e.target.value;
    setCategory({ ...category, [e.target.name]: value });
  };

  const saveChanges = () => {
    const file = fileRef.current.files[0];
    updateCategory({ file, category });
    toogleEdit();
  };

  return (
    <tr>
      <td>
        {edit ? (
          <>
            <button className="m-10" onClick={saveChanges}>
              Guardar
            </button>
            <button className="m-10" onClick={toogleEdit}>
              Salir
            </button>
          </>
        ) : (
          <button onClick={toogleEdit}>Editar</button>
        )}
      </td>
      <td>
        <img src={category.icon} alt="..." />
        {edit && <FileUploader forwardedRef={fileRef} />}
      </td>
      <td>
        <input
          name="id"
          value={category.id}
          onChange={handleChange}
          disabled={!edit}
        />
      </td>
      <td>
        <textarea
          className="p-10"
          name="name"
          value={category.name}
          onChange={handleChange}
          disabled={!edit}
        />
      </td>
    </tr>
  );
};

export default memo(CategoryEditor);
