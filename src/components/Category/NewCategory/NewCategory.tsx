import { memo, useRef, useState } from "react";
import FileUploader from "../../FileUploader";
import { category } from "../../../helpers/types";

interface Props {
  createCategory: Function;
}

const NewCategory = ({ createCategory }: Props) => {
  const [category, setCategory] = useState<category>({
    id: "",
    name: "",
    icon: "",
  });

  const fileRef = useRef<any>(null);

  const handleChange = (e: any) => {
    const value = e.target.value;
    setCategory({ ...category, [e.target.name]: value });
  };

  const saveChanges = () => {
    const file = fileRef.current.files[0];
    createCategory({ file, category });
  };

  return (
    <tr>
      <td>
        <button onClick={saveChanges}>Agregar</button>
      </td>
      <td>
        <FileUploader forwardedRef={fileRef} />
      </td>
      <td>
        <textarea name="id" value={category.id} onChange={handleChange} />
      </td>
      <td>
        <input name="name" value={category.name} onChange={handleChange} />
      </td>
    </tr>
  );
};

export default memo(NewCategory);
