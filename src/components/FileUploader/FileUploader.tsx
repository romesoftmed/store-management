import { memo } from "react";

interface Props {
  forwardedRef: any;
}

const FileUploader = ({ forwardedRef }: Props) => {
  return (
    <div>
      <form>
        <input
          ref={forwardedRef}
          className="m-10 file-input"
          type="file"
          accept="image/png, image/jpeg"
        />
      </form>
    </div>
  );
};

export default memo(FileUploader);
