import { memo } from "react";
import { category } from "../../../helpers/types";

interface Props {
  edit: boolean;
  categories: any;
  category: string;
  handleChange: any;
}

const Selector = ({ edit, categories, category, handleChange }: Props) => {
  return (
    <select
      name="category"
      disabled={!edit}
      onChange={handleChange}
      value={category}
    >
      {categories &&
        categories.map((category: category) => (
          <option key={category.id} value={category.id}>
            {category.name}
          </option>
        ))}
    </select>
  );
};

export default memo(Selector);
