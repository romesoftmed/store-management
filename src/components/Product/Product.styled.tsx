import styled from "styled-components";
import Product from "./Product";

const ProductStyled = styled(Product)`
  height: calc(95vh - 30px);
  margin: 10px;
  display: grid;

  h1 {
    text-align: center;
  }

  .product-navbar {
    padding: 5px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
  }

  .table-container {
    height: 100%;
    overflow-y: scroll;
  }

  table,
  th,
  td {
    padding: 4px;
    text-align: justify;
    border: 1px solid #e7e7e7;
  }

  td {
    text-align: center;
  }

  input {
    width: calc(100% - 10px);
  }

  .file-input {
    width: auto;
  }

  textarea {
    width: 150px;
    height: 80px;
  }

  img {
    width: 60px;
  }
`;

export default ProductStyled;
