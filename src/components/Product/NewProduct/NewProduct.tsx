import { memo, useRef, useState } from "react";
import FileUploader from "../../FileUploader";
import { defaultProduct } from "../../../helpers/templates";
import { product } from "../../../helpers/types";
import { getPrice } from "../../../helpers/functions";
import Selector from "../CategorySelector";

const NewProduct = (props: any) => {
  const [product, setProduct] = useState<product>(defaultProduct);
  const fileRef = useRef<any>(null);

  const { sku, name, size, unit, cost, price, discount, stock, category } =
    product;

  const handleChange = (e: any) => {
    setProduct({ ...product, [e.target.name]: e.target.value });
  };

  const handlePrice = (e: any) => {
    const params = { percentage: e.target.value, cost };
    setProduct({ ...product, price: getPrice(params) });
  };

  const saveChanges = () => {
    const file = fileRef.current.files[0];
    props.createProduct({ file, product });
  };

  return (
    <tr>
      <td>
        <button onClick={saveChanges}>Agregar</button>
      </td>
      <td>
        <input name="sku" value={sku} onChange={handleChange} />
      </td>
      <td>
        <FileUploader forwardedRef={fileRef} />
      </td>
      <td>
        <textarea name="name" value={name} onChange={handleChange} />
      </td>
      <td>
        <input name="size" type="number" value={size} onChange={handleChange} />
      </td>
      <td>
        <input name="unit" value={unit} onChange={handleChange} />
      </td>
      <td>
        <input name="cost" type="number" value={cost} onChange={handleChange} />
      </td>
      <td>
        <input type="number" value={price} disabled />
      </td>
      <td>
        <input type="number" onChange={handlePrice} />
      </td>
      <td>
        <input
          name="discount"
          type="number"
          value={discount}
          onChange={handleChange}
        />
      </td>
      <td>
        <input
          name="stock"
          type="number"
          value={stock}
          onChange={handleChange}
        />
      </td>
      <td>
        <Selector
          categories={props.categories}
          category={category}
          handleChange={handleChange}
          edit
        />
      </td>
    </tr>
  );
};

export default memo(NewProduct);
