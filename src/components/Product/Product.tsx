import { memo, useEffect, useState } from "react";
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
import useProduct from "./useProduct";
import NewProduct from "./NewProduct";
import ProductEditor from "./ProductEditor";
import { storage } from "../../helpers/firebase";
import { createProduct, updateProduct } from "../../api/product";
import Selector from "./CategorySelector";
import { category } from "../../helpers/types";
import { defaultCategory } from "../../helpers/templates";
import { getAllCategories } from "../../api/category";

interface Props {
  className?: string;
}

const Product = ({ className }: Props) => {
  const [categories, setCategories] = useState<category[]>();
  const [category, setCategory] = useState<category>(defaultCategory);
  const { products, status } = useProduct(category.id);

  useEffect(() => {
    getAllCategories()
      .then((categories) => {
        setCategories(categories);
        setCategory(categories[3]);
      })
      .catch(() => {});
  }, []);

  const create = ({ file, product }: any) => {
    if (file) {
      const storageRef = ref(storage, `product/${product.sku}.jpg`);
      uploadBytes(storageRef, file).then(() => {
        getDownloadURL(storageRef).then((img) => {
          createInDatabase({ ...product, img });
        });
      });
    }
  };

  const update = ({ file, product }: any) => {
    if (file) {
      const storageRef = ref(storage, `product/${product.sku}.jpg`);
      uploadBytes(storageRef, file).then(() => {
        updateInDatabase(product);
      });
    } else {
      updateInDatabase(product);
    }
  };

  const createInDatabase = (product: any) => {
    createProduct(product)
      .then(() => {})
      .catch(() => {});
  };

  const updateInDatabase = (product: any) => {
    const { sku, ...productBody } = product;
    updateProduct({ sku, product: productBody })
      .then(() => {})
      .catch(() => {});
  };

  const selectCategory = (e: any) => {
    if (categories) {
      const category = categories.find((c) => c.id === e.target.value);
      if (category) {
        setCategory(category);
      }
    }
  };

  return (
    <div className={className}>
      <div className="product-navbar">
        <h3>Gestionar productos</h3>
        <Selector
          categories={categories}
          category={category.id}
          handleChange={selectCategory}
          edit
        />
      </div>
      {status === "loading" && <h1>Cargando ...</h1>}
      {status === "error" && <h1>Hubo un error</h1>}
      {status === "loaded" && products && (
        <div className="table-container">
          <table>
            <tbody>
              <tr>
                <th></th>
                <th>SKU</th>
                <th>Imagen</th>
                <th>Nombre</th>
                <th>Cantidad</th>
                <th>Unidad</th>
                <th>Costo</th>
                <th>Precio</th>
                <th>Ganancia</th>
                <th>Descuento %</th>
                <th>Stock</th>
                <th>Categoría</th>
              </tr>
              <NewProduct categories={categories} createProduct={create} />
              {products.map((product) => (
                <ProductEditor
                  key={product.sku}
                  {...product}
                  categories={categories}
                  updateProduct={update}
                />
              ))}
            </tbody>
          </table>
        </div>
      )}
    </div>
  );
};

export default memo(Product);
