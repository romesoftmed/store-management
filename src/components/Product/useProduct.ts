import { useEffect, useState } from "react";
import { getProductsByCategory } from "../../api/product";
import { product } from "../../helpers/types";

const useProduct = (categoryId: string) => {
  const [products, setProducts] = useState<product[]>();
  const [status, setStatus] = useState<string>("init");

  useEffect(() => {
    setStatus("loading");
    getProductsByCategory({ categoryId })
      .then((products) => {
        setProducts(products);
        setStatus("loaded");
      })
      .catch(() => {
        setStatus("error");
      });
    // eslint-disable-next-line
  }, [categoryId]);

  return { products, status };
};

export default useProduct;
