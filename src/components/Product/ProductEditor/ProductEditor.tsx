import { memo, useEffect, useRef, useState } from "react";
import FileUploader from "../../FileUploader";
import { defaultProduct } from "../../../helpers/templates";
import { product } from "../../../helpers/types";
import { getPercentage, getPrice } from "../../../helpers/functions";
import Selector from "../CategorySelector";

const ProductEditor = (props: any) => {
  const [product, setProduct] = useState<product>(defaultProduct);
  const [edit, setEdit] = useState<boolean>(false);

  const fileRef = useRef<any>(null);

  useEffect(() => {
    setProduct(props);
    // eslint-disable-next-line
  }, []);

  const { sku, name, size, unit, cost, price, img, discount, stock, category } =
    product;

  const handleChange = (e: any) => {
    let value = e.target.value;

    if (e.target.name === "price") {
      const params = { percentage: value, cost };
      value = getPrice(params);
    }

    setProduct({ ...product, [e.target.name]: value });
  };

  const saveChanges = () => {
    const file = fileRef.current.files[0];
    props.updateProduct({ file, product });
    toogleEdit();
  };

  const toogleEdit = () => {
    setEdit(!edit);
  };

  return (
    <tr>
      <td>
        {edit ? (
          <>
            <button className="m-10" onClick={saveChanges}>
              Guardar
            </button>
            <button className="m-10" onClick={toogleEdit}>
              Salir
            </button>
          </>
        ) : (
          <button onClick={toogleEdit}>Editar</button>
        )}
      </td>
      <td>
        <p>{sku}</p>
      </td>
      <td>
        <img src={img} alt="..." />
        {edit && <FileUploader forwardedRef={fileRef} />}
      </td>
      <td>
        <textarea
          className="p-10"
          name="name"
          value={name}
          onChange={handleChange}
          disabled={!edit}
        />
      </td>
      <td>
        <input
          name="size"
          type="number"
          value={size}
          onChange={handleChange}
          disabled={!edit}
        />
      </td>
      <td>
        <input
          name="unit"
          value={unit}
          onChange={handleChange}
          disabled={!edit}
        />
      </td>
      <td>
        <input
          name="cost"
          type="number"
          value={cost}
          onChange={handleChange}
          disabled={!edit}
        />
      </td>
      <td>
        <input type="number" value={price} disabled />
      </td>
      <td>
        {edit ? (
          <input name="price" type="number" onChange={handleChange} />
        ) : (
          <p>{getPercentage({ price, cost })}</p>
        )}
      </td>
      <td>
        <input
          name="discount"
          type="number"
          value={discount}
          onChange={handleChange}
          disabled={!edit}
        />
      </td>
      <td>
        <input
          name="stock"
          type="number"
          value={stock}
          onChange={handleChange}
          disabled={!edit}
        />
      </td>
      <td>
        <Selector
          categories={props.categories}
          category={category}
          handleChange={handleChange}
          edit={edit}
        />
      </td>
    </tr>
  );
};

export default memo(ProductEditor);
