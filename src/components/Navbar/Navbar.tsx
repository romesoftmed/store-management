import { memo } from "react";
import { NavLink } from "react-router-dom";

interface Props {
  className?: string;
}

const Navbar = ({ className }: Props) => {
  return (
    <div className={className}>
      <NavLink to="/producto" className="nav-link" activeClassName="selected">
        <small>Productos</small>
      </NavLink>
      <NavLink to="/categoria" className="nav-link" activeClassName="selected">
        <small>Categorías</small>
      </NavLink>
    </div>
  );
};

export default memo(Navbar);
