import styled from "styled-components";
import Navbar from "./Navbar";

const NavbarStyled = styled(Navbar)`
  height: 5vh;
  margin: 10px;
  display: flex;
  flex-direction: row;

  .nav-link {
    margin: 5px;
    font-weight: bold;
    text-decoration: none;
    color: #000000;
  }

  .selected {
    color: #009e00;
  }
`;

export default NavbarStyled;
