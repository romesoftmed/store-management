import { BrowserRouter, Switch, Route } from "react-router-dom";
import Product from "./components/Product";
import Category from "./components/Category";
import Navbar from "./components/Navbar";

const App = () => {
  return (
    <BrowserRouter>
      <Navbar />
      <Switch>
        <Route path="/producto" component={Product}></Route>
        <Route path="/categoria" component={Category}></Route>
      </Switch>
    </BrowserRouter>
  );
};

export default App;
